PASSWORD_PATH = ".password"
PASSWORD_ID_PATH = ".password_id"


Vagrant.configure("2") do |config|

  config.vm.box = "trusty"
  config.vm.box_url = "https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"
  config.vm.box_check_update = false
  config.vm.hostname = "secure"
  config.vagrant.plugins = ["vagrant-hosts", "vagrant-vbguest","vagrant-triggers"]
  config.ssh.insert_key = false

  # forward ssh keys from main host - handy for gerrit and github
  config.ssh.forward_agent = true

  config.trigger.before :up do
     if File.exists?(PASSWORD_ID_PATH)
       password_id = File.read(PASSWORD_ID_PATH).strip
       print "The VM is encrypted, please enter the password\n#{password_id}: "
       password = STDIN.noecho(&:gets).strip
       File.write(PASSWORD_PATH, password)
       puts ""
     end
  end

  config.trigger.after :up do
      File.delete(PASSWORD_PATH) if File.exists?(PASSWORD_PATH)
    end

    config.trigger.after :destroy do
      File.delete(PASSWORD_ID_PATH) if File.exists?(PASSWORD_ID_PATH)
  end

  config.vm.provider "virtualbox" do |v|

    v.name = "secure"
    v.gui = false
    v.customize ["modifyvm", :id, "--cpus", "4"]
    v.customize ["modifyvm", :id, "--memory", "6000"]
    # enable serial port just in case vagrant image does not
    v.customize ["modifyvm", :id, "--uart1", "0x3F8", 4]

    if File.exists?(PASSWORD_ID_PATH)
      password_id = File.read(PASSWORD_ID_PATH).strip
      vb.customize "post-boot", [
        "controlvm", :id, "addencpassword", password_id, PASSWORD_PATH, "--removeonsuspend", "yes"
      ]
    end

    file_to_disk = 'disk2.vdi'
    unless File.exist?(file_to_disk)
      # 50 GB
      v.customize ['createhd', '--filename', file_to_disk, '--size', 50 * 1024]
    end
    v.customize ['storageattach', :id, '--storagectl', 'SATAController', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]

  end

end
